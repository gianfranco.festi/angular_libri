import { Component } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { Observable } from 'rxjs';
import { Libro } from './libro';
import { LibriService } from './service/libri.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  libri$?: Observable<Libro[]>;
  visualizza: boolean = false;
  myForm?: FormGroup;
  constructor(  public libriService: LibriService,
                public fb: FormBuilder) {

    this.libri$ = libriService.getAll();
    this.myForm = fb.group({
      id: [''],
      titolo: [''],
      autore: [''],
      prezzoCopertina: ['']
    })
  }

  visualizzaAdd() {
    this.visualizza = true;
  }
  annulla() {
    this.visualizza = false;
  }
  onSubmit(libro: Libro) {
    console.log(libro)
    this.libriService.post(libro).subscribe(res => {
      console.log(res);
      this.visualizza = false;
    })
  }
}
