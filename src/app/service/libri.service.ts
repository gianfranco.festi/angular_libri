import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Libro } from '../libro';

@Injectable({
  providedIn: 'root'
})
export class LibriService {
  url : string =  "http://liag.dynu.net:9999/api/libri";  
  constructor(public http: HttpClient) { 
  }

  getAll(): Observable<Libro[]>{
    return this.http.get<Libro[]>(this.url);
  }
  post(libro:Libro){
    return this.http.post(this.url,libro);
  }
}
